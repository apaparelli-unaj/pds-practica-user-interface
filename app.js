const express = require('express');
const app = express();
const path = require('path');
const port = 3000
const router = express.Router();

app.use(express.static(__dirname + '/static'));

router.get('/',function(req, res){
    res.sendFile(path.join(__dirname+'/index.html'));
});

router.get('/menu',function(req, res){
    res.sendFile(path.join(__dirname+'/menu.html'));
});

router.get('/carrito',function(req, res){
    res.sendFile(path.join(__dirname+'/carrito.html'));
});

// Router
app.use('/', router);
app.use('/header', router);

app.listen(port, () => console.log(`Server listening at http://localhost:${port}`))