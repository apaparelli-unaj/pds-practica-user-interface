/* Código para cargar el menu */

fetch('/menu')
.then(response => {
    return response.text()
}) 
.then(data => {
    document.querySelector("header").innerHTML = data;

    /* obtener o crear localstorage de productos y actualizar contador */
    if (!localStorage.getItem('items')){
        let items = [];
        localStorage.setItem('items', JSON.stringify(items));
    }
    var items = JSON.parse(localStorage.getItem('items'));

    let contador_carrito = items.length;
        document.getElementById('contador-carrito').innerHTML = contador_carrito;
});