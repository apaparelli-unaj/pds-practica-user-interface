/* Obtiene todos los productos */
fetch('http://localhost:5000/api/Producto', {
    method: 'GET',
    mode: 'cors'
})
.then(response => {
    return response.json()
})
.then(function(productos) {
    productos.forEach(element => {
        prod = createProduct(element);
        items = document.getElementById('items')
        items.innerHTML += prod;
    });
})
.catch(err => console.log('ERROR: ' + err));

/* Crea el html para un producto */
function createProduct(data){
    var esta_en_carrito = existeEnCarrito(parseInt(data.productoId));
    if (!esta_en_carrito){
        var btn_class = "success";
        var on_click = "AgregarProductoCarrito";
        var btn_texto = "Agregar"
    }
    else {
        var btn_class = "danger";
        var on_click = "EliminarProductoCarrito";
        var btn_texto = "Eliminar"
    }

    var item = 
        '<div class="col-md-3" id="producto-'+ data.productoId +'">' +
        '    <div class="card mt-4">' +
        '        <div class="card-title text-center">' +
        '            <h3>'+ data.nombre +'</h3>' +
        '            <span>'+ data.marca +'</span>' +
        '        </div>' +
        '        <div class="card-body text-center">' +
        '            <img src="/img/productos/' + data.imagen + '" class="mx-auto d-block" alt="' + data.nombre + '" />' +
        '            <span class="badge badge-pill badge-info">' +
        '                $ ' + data.precio + 
        '            </span>' +
        '        </div>' +
        '        <div class="card-footer text-center">' +
        '            <button' +
        '                class="btn btn-'+ btn_class +' float-right" onclick="'+ on_click +'(' + data.productoId + ')">' +
        '                ' + btn_texto +
        '            </button>' +
        '        </div>' +
        '    </div>' +
        '</div>'
    
    return item
};

function cambiarBoton(accion, productoId){
    let btn = document.getElementById('producto-'+productoId).getElementsByClassName('btn')[0];
    if (accion === "eliminar"){
        btn.classList.remove("btn-success");
        btn.classList.add("btn-danger");
        btn.innerHTML = "Eliminar";
        btn.onclick = function() {EliminarProductoCarrito(productoId)};
    }
    else if (accion === "agregar"){
        btn.classList.remove("btn-danger");
        btn.classList.add("btn-success");
        btn.innerHTML = "Agregar";
        btn.onclick = function() {AgregarProductoCarrito(productoId)};

    }
}

