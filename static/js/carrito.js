/* Agrega productos al carrito */
function AgregarProductoCarrito(productoId){
    var carrito = JSON.parse(localStorage.getItem('items'));
    carrito.push(productoId);
    localStorage.setItem('items', JSON.stringify(carrito));
    actualizarContador('sumar');
    cambiarBoton('eliminar', productoId);
};

/* Elimina productos al carrito */
function EliminarProductoCarrito(productoId){
    EliminarProducto(productoId);
    cambiarBoton('agregar', productoId);
};

function EliminarProducto(productoId){
    let carrito = JSON.parse(localStorage.getItem('items'));
    var nuevo_carrito = carrito.filter(function(value){return value != productoId});

    localStorage.setItem('items', JSON.stringify(nuevo_carrito));
    actualizarContador('restar');
}
/* suma  o resta 1 producto del contador de productos en el carrito 
   y actualiza localstorage 
*/
function actualizarContador(accion){
    let contador_el = document.getElementById('contador-carrito');
    let contador = parseInt(contador_el.innerText);

    let nuevo_contador = contador;
    if (accion === "sumar"){
        nuevo_contador = contador + 1;
    }
    else if(accion === "restar") {
        nuevo_contador = contador - 1;
    }
    localStorage.getItem("items");
    contador_el.innerHTML = nuevo_contador;
};

function existeEnCarrito(productoId){
    return JSON.parse(localStorage.getItem('items')).includes(productoId);
};

/* Obtener productos del carrito */
function obtenerProductosCarrito(){

    fetch('http://localhost:5000/api/Producto', {
        method: 'GET',
        mode: 'cors'
    })
    .then(response => {
        return response.json()
    })
    .then(function(productos) {
        var carrito = JSON.parse(localStorage.getItem('items'));

        productos_carrito = productos.filter(f => carrito.includes(f.productoId));

        productos_carrito.forEach(element => {
            prod = crearItemCarrito(element);
            items = document.getElementById('items-carrito')
            items.innerHTML += prod;
        });

        var total = productos_carrito.reduce((acumulado, p) => acumulado + p.precio, 0);
        document.getElementById('total-pagar').innerHTML = total;
    })
    .catch(err => console.log('ERROR: ' + err));
}

/* Crea el html para para los items del carrito */
function crearItemCarrito(data){
    var item = '' +
        '<tr id="row-'+data.productoId+'">' +
        '    <td class="p-2">' +
        '    <div class="media align-items-center">' +
        '        <img src="/img/productos/' + data.imagen + '" class="d-block ui-w-40 ui-bordered mr-4" alt="">' +
        '        <div class="media-body">' +
        '        <a href="#" class="d-block text-dark">' + data.nombre + '</a>' +
        '        <small>' +
        '            <span class="text-muted">Marca:</span> ' + data.marca +
        '            <span class="text-muted">Código: </span> ' + data.codigo +
        '        </small>' +
        '        </div>' +
        '    </div>' +
        '    </td>' +
        '    <td class="text-right font-weight-semibold align-middle p-4">$<span class="precio">'+data.precio+'</span></td>' +
        '    <td class="text-center align-middle px-0">' +
        '       <a href="#" class="shop-tooltip close float-none text-danger" onclick=eliminarFila('+ data.productoId +') data-original-title="Eliminar">×</a>' +
        '    </td>' +
        '</tr>';

    return item
};

function eliminarFila(producto){
    var precio = document.getElementById('row-'+producto).getElementsByClassName("precio")[0].textContent;
    var total = document.getElementById('total-pagar').textContent;
    document.getElementById('total-pagar').innerHTML = parseInt(total) - parseInt(precio);
    document.getElementById('row-'+producto).remove();
    EliminarProducto(producto);
}

function vaciarCarrito(){
    localStorage.removeItem('items');
    localStorage.setItem('items', JSON.stringify([]));
    document.location.href = '/';
}

function comprar(){
    var cliente = localStorage.getItem('cliente');
    var productos = JSON.parse(localStorage.getItem('items'));

    data = {
        'clienteId': parseInt(cliente),
        'productos': productos
    }

    fetch('http://localhost:5000/api/Venta', {
        method: 'POST',
        mode: 'cors',
        body: JSON.stringify(data),
        headers: {
            'Content-Type': 'application/json'
          },
    })
    .then(response => {
        return response.json()
    })
    .then(function(venta) {
        localStorage.removeItem('items');
        localStorage.setItem('items', JSON.stringify([]));
        page = document.getElementById('carrito');
        page.innerHTML = '' +
            '<div class="card">' +
            '   <h4>Gracias por su compra<h4>'+
            '   <h3>Su número de pedido es #'+ venta.ventaId +'<h3>'+
            '   <a class="btn btn-info pull-right" href="/">Volver a Listado de productos</a>' + 
            '</div>';
    })
    .catch(err => console.log('ERROR: ' + err));
}
