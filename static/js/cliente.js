function buscarCliente(){
    var dni = document.getElementById('dni').value;
    fetch('http://localhost:5000/api/Cliente?q='+dni, {
        method: 'GET',
        mode: 'cors'
    })
    .then(response => {
        return response.json()
    })
    .then(function(cliente) {
        if (cliente.length == 0){
            var msg = document.getElementById('cliente-msg');
            msg.innerHTML = "Cliente no valido";
        }
        else {
            datos = document.getElementById('datos-cliente').innerHTML = '' +
            '        <div class="media-body">' +
            '           <a href="#" class="d-block text-dark">Cliente: ' + cliente[0].nombre + ' ' + cliente[0].apellido+ '</a>' +
            '        <small>' +
            '            <span class="text-muted">DNI:</span> ' + cliente[0].dni +
            '            <span class="text-muted">Telefono:</span> ' + cliente[0].telefono +
            '            <span class="text-muted">Dirección: </span> ' + cliente[0].direccion +
            '        </small>' +
            '        </div>';

            document.getElementById('comprar').disabled = false;
            localStorage.setItem('cliente', cliente[0].clienteId);

        }
    })
    .catch(err => console.log('ERROR: ' + err));
}
